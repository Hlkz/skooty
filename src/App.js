import React from "react"
import Skooty from './Skooty'

function App() {
  return r('.App', [
		r('.navbar'),
		r(Skooty),
	])
}

export default App;
