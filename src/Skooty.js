import { useCallback } from 'react'
import { request } from './openai'

let lastMessageId = 0

let typeMessages = [
  undefined,
  'Comment me préparer ?',
  'Quoi faire ?',
  'Où manger ?',
  'Découverte',
  'Fête',
  'Culture',
]
let initMessages = []

function Skooty() {
  // Form state
  let [input, setInput] = useState('')
  let inputRef = useRef(input)
  inputRef.current = input
  let [type, setType] = useState(0)
  let typeRef = useRef(type)
  typeRef.current = type

  // Scroll à l'ajout de message
  let messagesElRef = useRef(null)
  let scrollDown = useCallback(() => {
    let messagesEl = messagesElRef.current
    if (messagesEl) messagesEl.scrollTo(0, messagesEl.scrollHeight)
  }, [])

  // Messages state
  let [messages, setMessages] = useState(initMessages)
  let addMessage = useCallback(({ role, content, type }) => {
    content = content.replace(/\n/g, '<br>')
    let newMessage = { id: ++lastMessageId, role, content, type }
    setMessages(messages => [...messages, newMessage])
    setTimeout(() => scrollDown())
  }, [])
  let didQueFaire = messages.some(e => e.role == 'assistant' && e.type == 2) // || true

  // Send via button or "Enter"
  let send = useCallback(params => {
    let { type } = params || {}
    let inputString = inputRef.current
    type = type || 0
    if (type)
      inputString = typeMessages[type]
    addMessage({ role: 'user', content: inputString, type })
    handleRequest({ inputString, type, addMessage }).catch(console.log)
    setInput('')
  }, [])

  // Handle "Enter" key
  useEffect(() => {
    let fn = handleKeyDown({ send })
    document.addEventListener('keydown', fn)
    return () => { document.removeEventListener('keydown', fn) }
  }, [])

  return r('.Skooty', [
    !!messages.length && r('.skootyAvatar.top'),
    r('.title', 'Skooty Chat'),
    r('.subtitle', 'Trajet Lyon - Amiens'),
    !messages.length && r('.skootyAvatar.middle'),
    r('.messages', { ref: messagesElRef }, [
      ...[...messages].map(message => r(Message, { key: message.id, message })),
    ]),
    r('.buttons', [
      r('button', { onClick: () => send({ type: 1 })}, typeMessages[1]),
      !didQueFaire && r('button', { onClick: () => send({ type: 2 })}, typeMessages[2]),
      r('button', { onClick: () => send({ type: 3 })}, typeMessages[3]),
    ]),
    didQueFaire && r('.buttons', [
      r.span('Que faire :'),
      r('button.green', { onClick: () => send({ type: 4 })}, typeMessages[4]),
      r('button.green', { onClick: () => send({ type: 5 })}, typeMessages[5]),
      r('button.green', { onClick: () => send({ type: 6 })}, typeMessages[6]),
    ]),
    r('.flex.w100', [
      r(MainInput, { input, setInput }),
      r('button.send', { onClick: send }),
    ])
  ])
}

function Message(props) {
  let { message } = props
  let { role, content, type } = message
  let isMe = role == 'user'
  return r('.Message'+(isMe?'':'.isBot'), { dangerouslySetInnerHTML: { __html: content } })
}

function MainInput(props) {
  let { input, setInput } = props
  return r('textarea', { placeholder: "Exemple : Penser à tout avant de partir", onChange: e => setInput(e.target.value), value: input })
}

const handleKeyDown = ({ send }) => (e) => {
  if (e.key == 'Enter') {
    e.preventDefault()
    send()
  }
}

async function handleRequest({ inputString, type, addMessage }) {
  type = type || 0
  if (!inputString) return
  let { outputString } = await request({ inputString, type })
  addMessage({ role: 'assistant', content: outputString, type })
}

export default Skooty
