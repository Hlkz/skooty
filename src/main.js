import React from "react"
import ReactDOM from "react-dom"
import "./index.css"
import { r } from "@njb/hyperscript"
global.r = r
import App from "./App"
global.useEffect = React.useEffect
global.useState = React.useState
global.useRef = React.useRef

const root = ReactDOM.createRoot(document.getElementById("root"))
root.render(r(App))
