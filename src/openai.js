import { Configuration, OpenAIApi } from "openai"

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY
})
const openai = new OpenAIApi(configuration);

let history = []

let typePrompts = [
  undefined,
  'ETAPE 2 : Comment me préparer ?',
  'ETAPE 2 : Quoi faire ?',
  'ETAPE 2 : Où manger ?',
  'ETAPE 3 : Découverte',
  'ETAPE 3 : Fête',
  'ETAPE 3 : Culture',
]
async function request({ type, inputString, setHistory }) {

  console.log('request', { type, inputString })
  const userPrompt = inputString
  const messages = inputString
    ? [
        { role: 'user', content: contextPrompt1 },
        { role: 'user', content: `Here is the first request you should answer : "${userPrompt}"` },
      ]
    : [
        { role: 'user', content: base1 },
        { role: 'assistant', content: "J'ai compris" },
        { role: 'user', content: 'ETAPE 1 : Amiens. '+ typePrompts[type] },
      ].filter(e => e)

  const response = await openai.createChatCompletion({
    model: "gpt-3.5-turbo",
    messages: messages,
  })
  console.log('chatgpt request:', { messages, response })
  const outputString = response.data.choices[0].message.content

  history = [
    ...history,
    { type, inputString, response, outputString },
  ]
  return { outputString }
}

const contextPrompt1 = 'I want you to act as a travel guide. Every information you give me must me accurate. The destination of my travel is "Amiens". You are speaking in french only.'
const base1 = `Voici un rôle que je te donne. Quand tu as bien tout retenu, dis-moi juste : "j'ai tout compris". Mets-toi dans la peau d'une IA répondant dans le Chat Bot d'une application permettant de centraliser tous ses voyages. Si la requête ne parle pas de voyage, réponds à l'utilisateur que tu ne peux pas répondre. Ton objectif est de pouvoir donner des conseils, idées, anecdotes aux utilisateurs qui voyagent lorsqu'ils arrivent dans une ville. ETAPE 1 : la localisation de l'utilisateur te sera donnée et tu devras simplement présenter la ville d'un point de vue touristique en maximum 600 caractères. Une fois cette étape passée, l'utilisateur pourra cliquer sur 3 boutons différents. ETAPE 2 : Le premier bouton est "comment se préparer", il te faudra alors expliquer à l'utilisateur comment bien se préparer pour voyager dans cette destination. Le second bouton sera "quoi faire", ici il faudrait que tu lui fasses un petit topo des meilleurs activités à faire dans la ville. Le troisième bouton sera "où manger", là tu devras lui sélectionner quelques adresses où bien manger dans la ville. Si l'utilisateur a choisi le bouton "quoi faire", 3 nouveaux boutons apparaissent. ETAPE 3 : Le premier bouton est "fête", présente alors les activités plus festives et jeunes à l'utilisateur. Le second bouton est "culture", présente alors les activités plus culturelles et historiques. Le troisième bouton est "découverte", présente alors d'autres activités plus insolites auxquelles l'utilisateur n'aurait pas forcément pensé. As-tu compris ?`

export {
  request,
}
